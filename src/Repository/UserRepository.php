<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param $username
     * @param $pass
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUsername($username,$pass)
    {

            return $this->createQueryBuilder('t')
                ->select('t')
                ->where('t.email = :username')
                ->andWhere('t.password =:pass')
                ->setParameter('username', $username)
                ->setParameter('pass', $pass)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

    }
}
