<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AuthType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword()
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("index");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/ping", name="ping")
     * @param ApiContext $apiContext
     * @return Response
     */
    public function pingAction(ApiContext $apiContext)
    {
        try {
            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        return $this->render('index.html.twig', [
            'error' => 0
        ]);
    }

    /**
     * @Route("/error", name="index-error")
     */
    public function indexWithErrorAction()
    {
        return $this->render('index.html.twig', [
            'error' => 1
        ]);
    }


    /**
     * @Route("/auth", name="auth")
     * @Method({"GET","HEAD","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function authAction(UserRepository $userRepository, Request $request, ApiContext $apiContext)
    {
        $form = $this->createForm(AuthType::class);
        $form->handleRequest($request);
        $error = null;
        if ($form->isSubmitted()) {
            $data = $form->getData();
            $username = $data['email'];
            $pass = md5($data['password']) . md5($data['password'] . '2');



            try {
                if ($apiContext->clientExists($username, $username)) {
                    $authUser = $userRepository->findUsername($username, $pass);

                    if ($authUser === null) {
                        return $this->redirectToRoute("index-error");
                    }

                    $token = new UsernamePasswordToken($authUser,
                        null,
                        'main',
                        $authUser->getRoles());

                    $this->container
                        ->get('security.token_storage')
                        ->setToken($token);

                    $this->container
                        ->get('session')
                        ->set('_security_main', serialize($token));

                    return $this->render('welcome_page.html.twig', array(
                        'user' => $authUser,
                        'error' => $error
                    ));
                }

                return $this->redirectToRoute("index-error");
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }
        return $this->render('auth.html.twig', [
            'form' => $form->createView(),
            'error'=>$error
        ]);
    }

    /**
     * @Route("/social-auth", name="app_social-auth")
     */
    public function socialAuthAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);


        dump($user);

        return $this->render('index.html.twig', []);

    }

    /**
     * @Route("/qwe", name="qwe")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qweAction(ApiContext $apiContext)
    {
        $result = $apiContext->clientExists('some & passport', '123@123.ru');
        return new Response(var_export($result, true));
    }
}
